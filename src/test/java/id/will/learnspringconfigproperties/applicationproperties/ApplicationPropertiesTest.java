package id.will.learnspringconfigproperties.applicationproperties;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

@SpringBootTest
public class ApplicationPropertiesTest {

	@Autowired
	private Environment environment;
	
	
	@Test
	void testApplicationProperties() {
		String applicationName = environment.getProperty("application.name");
		
		Assertions.assertEquals("Learn Spring Boot", applicationName);
	}
	
}
