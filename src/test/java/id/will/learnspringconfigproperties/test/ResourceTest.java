package id.will.learnspringconfigproperties.test;

import java.io.IOException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;


public class ResourceTest {

	@Test
	void testResource() throws IOException {
		ClassPathResource resource = new ClassPathResource("/application.properties");
		
		Assertions.assertNotNull(resource);
		Assertions.assertTrue(resource.exists());
		Assertions.assertNotNull(resource.getFile());
		
	}
	
}
