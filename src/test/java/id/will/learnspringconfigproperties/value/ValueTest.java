package id.will.learnspringconfigproperties.value;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import lombok.Getter;

@SpringBootTest
@Getter
public class ValueTest {

	@Value("${application.name}")
	private String name;
	
	@Value("${application.version}")
	private Integer version;
	
	@Value("${application.production-mode}")
	private boolean productionMode;
	
	@Value("${JAVA_HOME}")
	private String javaHome;
	
	@Test
	void testValue() {
		Assertions.assertEquals("Learn Spring Boot", name);
		Assertions.assertEquals(2, version);
		Assertions.assertEquals(false, productionMode);
//		Assertions.assertEquals("C:\\Program Files\\Amazon Corretto\\jdk11.0.15_9", javaHome);
		
}
	
}
